import json
import sys

# categories.json holds a list of categories.
# each cat. has a name and a filepath.
# the filepath is the path of another json file with all the data for that category
# example:
# categories.json -> category -> category file -> subcategory -> items -> item
# categories.json -> "Classes" -> "/home/taco/.tono/classes.json" -> "Phys" -> "items" -> item


class Args:
    def __init__(self):
        self.argList = sys.argv
        self.argList.pop(0)

    def get(self):  # return whole list
        return self.argList

    def geti(
        self, index
    ):  # attempt to return an item, returns None if index is invalid
        if index > len(self.argList) or index < 0 or len(self.argList) == 0:
            return None
        return self.argList[index]


def printj(inJson):  # pretty print json
    print(json.dumps(inJson, indent=4))


def file_to_json(filepath):
    f = open(filepath, "r")
    fileJson = json.load(f)
    f.close()
    return fileJson


def write_json(inJson, filepath):
    f = open(filepath, "w")
    json.dump(inJson, f)
    f.close()


def write_string(instr, filepath):  # function to write a string to a file
    f = open(filepath, "w")
    f.write(instr)
    f.close()


def get_item(index, filepath):  # helper to get a specific item from a file
    fileJson = file_to_json(filepath)
    return fileJson[index]


def add_item(item, filepath):  # function to add a json item to a json file
    currentJson = file_to_json(filepath)
    currentJson.append(item)
    write_json(currentJson, filepath)


def delete_item(itemnum, filepath):  # function to directly delete an item
    localJson = file_to_json(filepath)
    localJson.pop(itemnum)
    write_json(localJson, filepath)


def ninput(prompt):  # get input, return None if input is ''
    result = input(prompt + ": ")
    if result == "" or result == " ":
        result = None
    return result


def defaulting_input(
    prompt, default
):  # helper to get input, but replace with a default value if input is empty
    result = ninput(prompt)
    if result != None:
        return result
    return default


def yes_or_no(prompt, default="y"):  # helper to use a Y/n style prompt
    promptStr = prompt + " [Y/n]"
    default_input = "y"
    if default != "y" and default != "n":
        print_err("Invalid Y/n prompt default")
    elif default == "n":
        promptStr = prompt + " [y/N]"
        default_input = "n"
    result = defaulting_input(promptStr, default_input)
    if result == "y" or result == "Y":
        return True
    else:
        return False


def req_input(prompt):  # helper for required interactive input fields
    result = ninput(prompt + " (required)")
    if not result:
        print_err("You must fill out required fields.")
    return result


def clamp(
    val, min, max
):  # clamp a number between some bounds. set min or max to None for no limit
    if min == None:
        min = val - 1  # min will always be smaller than val
    if max == None:
        max = val + 1  # max always bigger than val
    if val < min:
        return min
    if val > max:
        return max
    return val


def print_x_times(
    inStr, x
):  # function to print a string x number of times (no \n by default)
    i = 0
    while i < x:
        print(end=inStr)
        i += 1


def tprint(inVal, tabs):  # print inVal with tabs indentation
    print_x_times("\t", tabs)
    print(inVal + ": ")


def itprint(inVal, tabs):
    print_x_times("\t", tabs)
    print(inVal)
