#!/usr/bin/env python3

from helpers import *

catPath = "/home/taco/.tono/categories.json"

args = Args()


def list_categories(filepath, offset=0):  # recursively print category-item hierarchy
    catJson = file_to_json(filepath)
    for cat in catJson:
        tabs = offset  # set to zero, unless the recursive call says to add more tabs
        if cat.get("catName") and cat.get("catFile"):  # is a top level category
            tprint(cat.get("catName"), tabs)
            tabs += 1  # indent the next item
            list_categories(
                cat.get("catFile"), offset=tabs
            )  # calls itself, passing the current tab level to it
        elif cat.get("catName"):  # is just a normal category
            tprint(cat.get("catName"), tabs)
            if cat.get("items"):  # category has a list of items
                tabs += 1  # tab because we're going to print items
                for item in cat.get("items"):
                    itprint(item.get("message"), tabs)


list_categories(catPath)
